﻿using System.Collections.Generic;
using System.Drawing;

namespace  StealthNinja
{
    public class Enemy : GameObject
    {
        public Ray RayEntity { get; }
        public List<Bitmap> ListUp { get; set; }
        public List<Bitmap> ListDown { get; set; }
        public List<Bitmap> ListLeft { get; set; }
        public List<Bitmap> ListRight { get; set; }

        public Enemy(ID.ID_VAL id, int posX, int posY, double velX, double velY, List<KeyValuePair<Direction.Direction_Val, Bitmap>> textureImages)
            : base(id, posX, posY, velX, velY, textureImages)
        {
            this.ListUp = base.GetTextureByDirection(Direction.Direction_Val.UP);
            this.ListDown = base.GetTextureByDirection(Direction.Direction_Val.DOWN);
            this.ListLeft = base.GetTextureByDirection(Direction.Direction_Val.LEFT);
            this.ListRight = base.GetTextureByDirection(Direction.Direction_Val.RIGHT);
        }

        public Enemy(ID.ID_VAL id, int posX, int posY, double velX, double velY, List<KeyValuePair<Direction.Direction_Val, Bitmap>> textureImages, List<KeyValuePair<Direction.Direction_Val, Bitmap>> rayImages)
            : base(id, posX, posY, velX, velY, textureImages)
        {
            this.RayEntity = new Ray(ID.ID_VAL.RAY, posX, posY, 0, 0, rayImages);
            this.ListUp = base.GetTextureByDirection(Direction.Direction_Val.UP);
            this.ListDown = base.GetTextureByDirection(Direction.Direction_Val.DOWN);
            this.ListLeft = base.GetTextureByDirection(Direction.Direction_Val.LEFT);
            this.ListRight = base.GetTextureByDirection(Direction.Direction_Val.RIGHT);
        }

        public Enemy(ID.ID_VAL id, int posX, int posY, double velX, double velY,
            Bitmap image, Bitmap rayImage) : base(id, posX, posY, velX, velY, image)
        {
            this.RayEntity = new Ray(ID.ID_VAL.RAY, posX, posY, 0, 0, rayImage);
        }

        public class Ray : GameObject
        {
            public Ray(ID.ID_VAL id, int posX, int posY, double velX, double velY, List<KeyValuePair<Direction.Direction_Val, Bitmap>> rayList) : base(id, posX, posY, velX, velY, rayList)
            {
            }

            public Ray(ID.ID_VAL id, int posX, int posY, double velX, double velY, Bitmap rayImage) : base(id, posX, posY, velX, velY, rayImage)
            {
            }

            public override void Tick()
            {
            }
        }

        public override void Tick()
        {
            throw new System.NotImplementedException();
        }
    }
}