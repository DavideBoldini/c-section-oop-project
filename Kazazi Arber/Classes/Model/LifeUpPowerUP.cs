﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace StealthNinja
{
    public class LifeUpPowerUP : GameObject
    {
        public LifeUpPowerUP(ID.ID_VAL id, int posX, int posY, double velX, double velY, List<KeyValuePair<Direction.Direction_Val, Bitmap>> newImages)
         : base(id, posX, posY, velX, velY, newImages) { }


        public void Effect(Player player)
        {
            if (player.Health < Player.MAX_LIFES)
            {
                player.AddLife();
            }
        }

        public override void Tick()
        {
            throw new NotImplementedException();
        }
    }


}
