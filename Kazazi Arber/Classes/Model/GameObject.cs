﻿using System;
using System.Collections.Generic;
using System.Drawing;

//NOTA: Implementazione Parziale

namespace StealthNinja
{
    public abstract class GameObject : IGameObject
    {
        public bool Visible { get; set; }
        public KeyValuePair<int, int> Coordinates { get; set; }
        public KeyValuePair<double, double> Velocity { get; set; }
        public KeyValuePair<int, int> Dimension { get; set; }
        public ID.ID_VAL Id { get; }
        public Dictionary<Direction.Direction_Val, List<Bitmap>> ImagesDictionary { get; set; }

        public Bitmap Image { get; set; }

        public Rectangle Rect { get; }

        public GameObject(ID.ID_VAL id, int posX, int posY, double velX, double velY, List<KeyValuePair<Direction.Direction_Val, Bitmap>> newImages)
        {
            this.Id = id;
            this.Coordinates = new KeyValuePair<int, int>(posX, posY);
            this.Velocity = new KeyValuePair<double, double>(velX, velY);
            this.ImagesDictionary = new Dictionary<Direction.Direction_Val, List<Bitmap>>();
            this.FillDictionaryImages(newImages);
            this.Rect = new Rectangle(Coordinates.Key, Coordinates.Value, 64, 64);
            this.Visible = true;
        }

        internal List<Bitmap> GetTextureByDirection(object uP)
        {
            throw new NotImplementedException();
        }

        //Altro costruttore con Immagine unica
        public GameObject(ID.ID_VAL id, int posX, int posY, double velX, double velY, Bitmap image)
        {
            this.Id = id;
            this.Coordinates = new KeyValuePair<int, int>(posX, posY);
            this.Velocity = new KeyValuePair<double, double>(velX, velY);
            this.Rect = new Rectangle(Coordinates.Key, Coordinates.Value, 64, 64);
            this.Visible = true;
        }

        protected GameObject(ID.ID_VAL iD)
        {
            Id = iD;
        }

        public void FillDictionaryImages(List<KeyValuePair<Direction.Direction_Val, Bitmap>> list)
        {

        }

        public List<Bitmap> GetListUP()
        {
            return this.ImagesDictionary[Direction.Direction_Val.UP];
        }

        public List<Bitmap> GetListDOWN()
        {
            return this.ImagesDictionary[Direction.Direction_Val.DOWN];
        }

        public List<Bitmap> GetListLEFT()
        {
            return this.ImagesDictionary[Direction.Direction_Val.LEFT];
        }

        public List<Bitmap> GetListRIGHT()
        {
            return this.ImagesDictionary[Direction.Direction_Val.RIGHT];
        }

        public List<Bitmap> GetTextureByDirection(Direction.Direction_Val direction)
        {
            List<Bitmap> finalList = new List<Bitmap>();
            foreach (KeyValuePair<Direction.Direction_Val, List<Bitmap>> entry in ImagesDictionary)
            {
                if (entry.Key.Equals(direction))
                {
                    finalList.AddRange(entry.Value);
                }
            }
            return finalList;
        }

        public abstract void Tick();
    }
}