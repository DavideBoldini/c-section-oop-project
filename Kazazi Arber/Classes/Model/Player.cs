﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace StealthNinja
{
    public class Player : GameObject
    {
        public static readonly int MAX_LIFES = 3;

        public double Speed { get; set; }
        public int Health { get; set; }
        public bool Recovering { get; set; }
        public Player(ID.ID_VAL id, int posX, int posY, double velX, double velY, List<KeyValuePair<Direction.Direction_Val, Bitmap>> newImages) : base(id, posX, posY, velX, velY, newImages)
        {
            Health = MAX_LIFES;
            Recovering = false;
            Speed = 2.0;
        }

        public void RemoveLife()
        {
            if (Health > 0 && Visible)
            {
                Health--;
                Recovering = true;
            }
        }

        public void AddLife()
        {
            Health++;
        }

        public override void Tick()
        {
            throw new System.NotImplementedException();
        }
    }
}