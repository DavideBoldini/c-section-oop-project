﻿using System.Collections.Generic;
using System.Drawing;

namespace StealthNinja
{
    internal interface IGameObject
    {
        void Tick();
        void FillDictionaryImages(List<KeyValuePair<Direction.Direction_Val, Bitmap>> list);
    }
}