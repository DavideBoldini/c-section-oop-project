﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace StealthNinja
{
    public class InvisiblePowerUP : GameObject

    {
        public InvisiblePowerUP(ID.ID_VAL id, int posX, int posY, double velX, double velY, List<KeyValuePair<Direction.Direction_Val, Bitmap>> newImages)
         : base(id, posX, posY, velX, velY, newImages) { }

        public void Effect(Player player)
        {
            player.Visible = false;
        }

        public void Terminate(Player player)
        {
            player.Visible = true;
        }
        public override void Tick()
        {
            throw new NotImplementedException();
        }
    }
}
