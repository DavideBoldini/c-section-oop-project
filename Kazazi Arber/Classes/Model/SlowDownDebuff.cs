﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;


namespace StealthNinja
{
    public class SlowDownDebuff : GameObject
    {
        const double divConstraint = 2;
        double OldSpeed { get; set; }

        public SlowDownDebuff(ID.ID_VAL id, int posX, int posY, double velX, double velY, List<KeyValuePair<Direction.Direction_Val, Bitmap>> newImages)
         : base(id, posX, posY, velX, velY, newImages) { }

        public void Effect(Player player)
        {
            OldSpeed = player.Speed;
            player.Speed = OldSpeed / divConstraint;

        }

        public void Terminate(Player player)
        {
            player.Speed = OldSpeed;

        }

        public override void Tick()
        {
            throw new NotImplementedException();
        }
    }
}
