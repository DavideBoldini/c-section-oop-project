﻿using System.Collections.Generic;

namespace StealthNinja
{
    public class Collisions : ICollisions
	{
        public bool CheckCollisions(GameObject obj1, GameObject obj2)
        {
            return obj1.Rect.IntersectsWith(obj2.Rect);
        }

        public void CollisionsWall(GameObject obj1, GameObject obj2)
        {
            if (this.CheckCollisions(obj1, obj2))
            {
                int posX = (int)(obj1.Coordinates.Key - obj1.Velocity.Key);
                int posY = (int)(obj1.Coordinates.Value - obj1.Velocity.Value);
                obj1.Coordinates = new KeyValuePair<int,int> (posX, posY);
            }
        }
    }
}