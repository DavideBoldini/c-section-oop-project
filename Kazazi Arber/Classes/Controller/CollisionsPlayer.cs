﻿using System;

namespace StealthNinja
{
    public class CollisionsPlayer : Collisions, ICollisionsPlayer
    {
        public void CheckCollisionsInGame(Player obj1, GameObject obj2)
        {
            if (CheckCollisions(obj1, obj2) && obj2.Visible)
            {
                if (obj2.Id.Equals(ID.ID_VAL.WALL))
                {
                    CollisionsWall(obj1, obj2);
                }
                else if (obj2.Id.Equals(ID.ID_VAL.ENEMY))
                {
                    this.CollisionsEnemy(obj1);
                }
                else if (obj2.Id.Equals(ID.ID_VAL.INVISIBLEPU))
                {
                    this.CollisionsInvisible(obj1, (InvisiblePowerUP)obj2);
                }
                else if (obj2.Id.Equals(ID.ID_VAL.LIFEUPPU))
                {
                    this.CollisionsLifeUp(obj1, (LifeUpPowerUP)obj2);
                }
                else if (obj2.Id.Equals(ID.ID_VAL.FIREDB))
                {
                    this.CollisionsFire(obj1, (FireDebuff)obj2);
                }
                else if (obj2.Id.Equals(ID.ID_VAL.SLOWDOWNDB))
                {
                    this.CollisionsSlowDown(obj1, (SlowDownDebuff)obj2);
                }

            }
        }

        private void CollisionsSlowDown(Player obj1, SlowDownDebuff obj2)
        {
            obj2.Effect(obj1);
        }

        private void CollisionsFire(Player obj1, FireDebuff obj2)
        {
            obj2.Effect(obj1);
        }

        private void CollisionsLifeUp(Player obj1, LifeUpPowerUP obj2)
        {
            obj2.Effect(obj1);
        }

        private void CollisionsInvisible(Player obj1, InvisiblePowerUP obj2)
        {
            obj2.Effect(obj1);
        }

        private void CollisionsEnemy(Player obj1)
        {
            obj1.RemoveLife();
        }
    }
}