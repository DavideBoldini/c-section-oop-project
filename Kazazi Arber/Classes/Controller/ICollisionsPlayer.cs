﻿using System;

namespace StealthNinja
{
    public interface ICollisionsPlayer
    {

        void CheckCollisionsInGame(Player obj1, GameObject obj2);

    }
}
