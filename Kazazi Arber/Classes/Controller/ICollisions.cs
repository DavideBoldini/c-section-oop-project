﻿using System;

namespace StealthNinja
{
    public interface ICollisions
    {

        Boolean CheckCollisions(GameObject obj1, GameObject obj2);

        void CollisionsWall(GameObject obj1, GameObject obj2);

    }
}
