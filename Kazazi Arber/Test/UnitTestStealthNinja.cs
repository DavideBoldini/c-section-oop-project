using NUnit.Framework;
using StealthNinja;

namespace NUnitStealthNinja
{
    public class Tests
    {
        Enemy enemy;
        Player player;
        CollisionsPlayer collisionsPlayer;
        [SetUp]
        public void Setup()
        {
            enemy = new Enemy(ID.ID_VAL.ENEMY, 0, 0, 0, 0, null);
            player = new Player(ID.ID_VAL.PLAYER, 0, 0, 0, 0, null);
            collisionsPlayer = new CollisionsPlayer();
        }

        [Test]
        public void TestCollisionEnemy()
        {
            //Verifico che le caratteristiche principali del Player siano rispettate
            Assert.IsTrue(player.Visible);
            Assert.AreEqual(player.Health, 3);
            //Faccio in modo che avvenga la collisione con un nemico
            collisionsPlayer.CheckCollisionsInGame(player, enemy);
            //Verifico che siano state apportate gli effetti causati dalla collisione
            Assert.AreEqual(player.Health, 2);
            Assert.IsTrue(player.Recovering);
            //Riporto il Player allo stato iniziale
            player.AddLife();
            player.Recovering = false;
            player.Visible = true;
            Assert.AreEqual(player.Health, 3);
        }

        [Test]
        public void TestCollisionLifeUpPowerUP()
        {
            //Verifico che le vite del Player siano tre
            Assert.AreEqual(player.Health, 3);
            LifeUpPowerUP lifeUp = new LifeUpPowerUP(ID.ID_VAL.LIFEUPPU, 0, 0, 0, 0, null);
            //Verifico che seppur il Player collida con il lifeUP, non si eccede il numero massimo di vite
            collisionsPlayer.CheckCollisionsInGame(player, lifeUp);
            Assert.AreNotEqual(player.Health, 4);
            collisionsPlayer.CheckCollisionsInGame(player, enemy);
            Assert.AreEqual(player.Health, 2);
            //Verifico che in seguito alla collisione, la vita precedentemente persa sia ristabilita
            collisionsPlayer.CheckCollisionsInGame(player, lifeUp);
            Assert.AreEqual(player.Health, 3);
        }

        [Test]
        public void TestCollisionFireDebuff()
        {
            FireDebuff fireDebuff = new FireDebuff(ID.ID_VAL.FIREDB, 0, 0, 0, 0, null);
            collisionsPlayer.CheckCollisionsInGame(player, fireDebuff);
            Assert.AreEqual(player.Health, 2);
            collisionsPlayer.CheckCollisionsInGame(player, fireDebuff);
            Assert.AreEqual(player.Health, 1);
            collisionsPlayer.CheckCollisionsInGame(player, fireDebuff);
            Assert.AreEqual(player.Health, 0);
            //Verifico che il fuoco non mi porti ad avere un numero di vite negative
            collisionsPlayer.CheckCollisionsInGame(player, fireDebuff);
            Assert.AreEqual(player.Health, 0);
            //Riporto il Player allo stato iniziale
            player.Health = 3;
        }

        [Test]
        public void TestCollisionInvisiblePowerUP()
        {
            InvisiblePowerUP invisiblePowerUP = new InvisiblePowerUP(ID.ID_VAL.INVISIBLEPU, 0, 0, 0, 0, null);
            //Verifico che il Player sia diventato effettivamente invisibile
            collisionsPlayer.CheckCollisionsInGame(player, invisiblePowerUP);
            Assert.IsFalse(player.Visible);
            //Verifico che in seguito ad una collisione con un nemico la vita non sia stata sottratta
            collisionsPlayer.CheckCollisionsInGame(player, enemy);
            Assert.AreEqual(player.Health, 3);
            invisiblePowerUP.Terminate(player);
            Assert.IsTrue(player.Visible);
            //Una volta terminato l'effetto, verifico che la collisione con l'enemy avvenga regolarmente
            collisionsPlayer.CheckCollisionsInGame(player, enemy);
            Assert.AreEqual(player.Health, 2);
            collisionsPlayer.CheckCollisionsInGame(player, invisiblePowerUP);
            collisionsPlayer.CheckCollisionsInGame(player, enemy);
            Assert.AreEqual(player.Health, 2);
            invisiblePowerUP.Terminate(player);
            //Riporto il Player allo stato iniziale
            player.AddLife();
        }

        [Test]
        public void TestCollisionSlowDownDebuff()
        {
            SlowDownDebuff slowDownDebuff = new SlowDownDebuff(ID.ID_VAL.SLOWDOWNDB, 0, 0, 0, 0, null);
            //Verifico che la velocit� sia quella attesa in condizioni normali
            Assert.AreEqual(player.Speed, 2.0);
            collisionsPlayer.CheckCollisionsInGame(player, slowDownDebuff);
            //Verifico che la velocit� sia stata dimezzata
            Assert.AreEqual(player.Speed, 1.0);
            slowDownDebuff.Terminate(player);
            //Verifico che la velocit� non sia 0.5 
            collisionsPlayer.CheckCollisionsInGame(player, slowDownDebuff);
            Assert.AreNotEqual(player.Speed, 0.5);
            slowDownDebuff.Terminate(player);

        }
    }
}