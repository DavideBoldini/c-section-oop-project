﻿using System.Timers;
using Luca_Ferretti.Luca_Ferretti.Model;

namespace Luca_Ferretti.Luca_Ferretti.Controller
{
    public class LimitedTimer
    {
        public Timer Timer { get; set; }
        const int SLOW_SECS = 5;
        const int SPEEDUP_SECS = 5;
        const int INVISIBLE_SECS = 5;
        const int RECOVER_SECS = 2;
        const int KNIFE_SECS = 10;
        Player player;
        ID.Id_val idStaticObject;
        int secs;
        static LimitedTimerTask limitedTimerTask;

        public LimitedTimer(Player player, ID.Id_val ID)
        {
            this.player = player;
            this.idStaticObject = ID;
            this.secs = 0;
        }

        public void Start()
        {
            this.Timer = new System.Timers.Timer(1000);
            this.Timer.Elapsed += new ElapsedEventHandler(OnTimedEvent);
            this.Timer.Start();                                                 //manca freeze e eventuale RECOVER
            if (this.idStaticObject.Equals(ID.Id_val.INVISIBLEPU))
            {
                limitedTimerTask = new LimitedTimerTask(this, this.secs, INVISIBLE_SECS);
            }
            else if (this.idStaticObject.Equals(ID.Id_val.SLOWDOWNDB))
            {
                limitedTimerTask = new LimitedTimerTask(this, this.secs, SLOW_SECS);
            }
            else if (this.idStaticObject.Equals(ID.Id_val.SPEEDUPPU))
            {
                limitedTimerTask = new LimitedTimerTask(this, this.secs, SPEEDUP_SECS);
            }
            else if (this.idStaticObject.Equals(ID.Id_val.KNIFEPU))
            {
                limitedTimerTask = new LimitedTimerTask(this, this.secs, KNIFE_SECS);
            }

        }

        private static void OnTimedEvent(object source, ElapsedEventArgs e)
        {
            limitedTimerTask.Run();
        }


        public void StopLimitedTimer()
        {
            if (this.IsActiveStaticObject())
            {
                this.player.ActivePowerupDebuff.TerminateEffect(this.player);
            }
            this.Timer.Stop();
            this.Timer.Dispose();
            limitedTimerTask = null;
        }


        public void Pause()
        {
            if (this.IsActiveStaticObject())
            {
                this.secs = limitedTimerTask.Secs;
                this.secs = limitedTimerTask.GetSecs();       
            }
        }

            private bool IsActiveStaticObject()
        {
            return (this.idStaticObject.Equals(ID.Id_val.SLOWDOWNDB) || this.idStaticObject.Equals(ID.Id_val.SPEEDUPPU)
                    || this.idStaticObject.Equals(ID.Id_val.INVISIBLEPU) || this.idStaticObject.Equals(ID.Id_val.KNIFEPU));
        }
    }
}
