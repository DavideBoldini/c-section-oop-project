﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Timers;

namespace Luca_Ferretti.Luca_Ferretti.Controller
{
    public class LimitedTimerTask
    {
        LimitedTimer timer;
        public int Secs { get; set; }
        int stop;

        public LimitedTimerTask(LimitedTimer timer, int secs, int stop)
        {
            this.timer = timer;
            this.Secs = secs;
            this.stop = stop;
        }

       public void Run() {
            this.Secs = this.Secs + 1;
            if (this.Secs > this.stop)
            {
                this.timer.StopLimitedTimer();
            }
        }

        public int GetSecs() => this.Secs;
    }

}
