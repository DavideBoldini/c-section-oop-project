using NUnit.Framework;
using Luca_Ferretti.Luca_Ferretti.Model;
using Luca_Ferretti.Luca_Ferretti.Controller;

namespace ClassTests
{
    public class UnitTests
    {
        Player player;
        InvisiblePowerup invisiblePowerup;
        LifeUpPowerup lifeUpPowerup;
        SpeedUpPowerup speedUpPowerup;
        KnifePowerup knifePowerup;
        FireDebuff fireDebuff;
        SlowDownDebuff slowDownDebuff;

        [SetUp]
        public void Setup()
        {
            this.player = new Player(ID.Id_val.PLAYER, 2, 2);
        }

        [Test]
        public void TestInitialHealth()
        {
            Assert.AreEqual(this.player.health, 3);
        }

        [Test]
        public void TestHealthInGame()          //test sulla vita del player
        {
            Assert.AreEqual(this.player.health, 3);
            this.player.RemoveLife();
            Assert.AreEqual(this.player.health, 2);
            this.player.AddLife();
            Assert.AreEqual(this.player.health, 3);
        }

        [Test]
        public void TestGameOver()
        {
            Assert.AreEqual(this.player.health, 3);     //verifico il game over dopo aver perso tutte le vite
            this.player.RemoveLife();
            Assert.AreEqual(this.player.health, 2);
            this.player.RemoveLife();
            Assert.AreEqual(this.player.health, 1);
            this.player.RemoveLife();
            Assert.AreEqual(this.player.health, 0);     
            Assert.IsTrue(this.player.gameOver);
            this.player.AddLife();                  //ripristino la vita a 3
            this.player.AddLife();
            this.player.AddLife();
            Assert.AreEqual(this.player.health, 3);
        }

        [Test]
        public void TestInvisibility()
        {
            this.invisiblePowerup = new InvisiblePowerup(ID.Id_val.INVISIBLEPU);    
            this.invisiblePowerup.Effect(this.player);
            this.player.RemoveLife();        // non rimuove la vita visto che ha l'invisible powerup
            Assert.AreEqual(this.player.health, 3);
            this.player.ActivePowerupDebuff.TerminateEffect(this.player);
            this.player.RemoveLife();
            Assert.AreEqual(this.player.health, 2);
            this.player.AddLife();      //cos� in ogni test parto da 3 vite
        }

        [Test]
        public void TestLifeUpPowerup()
        {
            this.lifeUpPowerup = new LifeUpPowerup(ID.Id_val.LIFEUPPU);
            this.player.RemoveLife();
            Assert.AreEqual(this.player.health, 2);
            this.lifeUpPowerup.Effect(this.player);
            Assert.AreEqual(this.player.health, 3);
        }

        [Test]
        public void TestSpeedUpPowerup()
        {
            this.speedUpPowerup = new SpeedUpPowerup(ID.Id_val.SPEEDUPPU);
            Assert.AreEqual(this.player.speed, 2.0);
            this.speedUpPowerup.Effect(this.player);
            Assert.AreEqual(this.player.speed, 4.0);
            Assert.AreNotEqual(this.player.speed, 2.0);     //la velocit� � cambiata
            this.speedUpPowerup.TerminateEffect(player);
            Assert.AreEqual(this.player.speed, 2.0);
        }

        [Test]
        public void TestKnifePowerup()
        {
            this.knifePowerup = new KnifePowerup(ID.Id_val.KNIFEPU);
            this.knifePowerup.Effect(this.player);
            this.player.RemoveLife();
            Assert.AreEqual(this.player.health, 3);
            this.knifePowerup.TerminateEffect(this.player);
            this.player.RemoveLife();       //una volta terminato , il player torna a perdere vite
            Assert.AreEqual(this.player.health, 2);
            this.player.AddLife();
            Assert.AreEqual(this.player.health, 3);
        }

        [Test]
        public void TestFireDebuff()
        {
            this.fireDebuff = new FireDebuff(ID.Id_val.FIREDB);
            Assert.AreEqual(this.player.health, 3);
            this.fireDebuff.Effect(this.player);
            Assert.AreEqual(this.player.health, 2);
            this.player.AddLife();
            Assert.AreEqual(this.player.health, 3);
        }

        [Test]
        public void TestSlowDownDebuff()
        {
            this.slowDownDebuff = new SlowDownDebuff(ID.Id_val.SLOWDOWNDB);
            Assert.AreEqual(this.player.speed, 2.0);
            this.slowDownDebuff.Effect(this.player);
            Assert.AreEqual(this.player.speed, 1.0);
            Assert.AreNotEqual(this.player.speed, 2.0);     //la velocit� � cambiata
            this.slowDownDebuff.TerminateEffect(player);
            Assert.AreEqual(this.player.speed, 2.0);
        }

        /* Test per verificare il corretto funzionamento dei powerup e dei debuff implementati. 
         * Nello specifico, dato che il corretto funzionamento dei singoli � stato verificato nei precedenti test, verr� verificata 
         * una corretta alternanza tra coloro che fanno parte degli ActiveStaticObject, che come caratteristica si sostituiscono a vicenda.
         */

        [Test]
        public void TestGeneral()
        {
            this.invisiblePowerup = new InvisiblePowerup(ID.Id_val.INVISIBLEPU);
            this.lifeUpPowerup = new LifeUpPowerup(ID.Id_val.LIFEUPPU);
            this.speedUpPowerup = new SpeedUpPowerup(ID.Id_val.SPEEDUPPU);
            this.knifePowerup = new KnifePowerup(ID.Id_val.KNIFEPU);
            this.fireDebuff = new FireDebuff(ID.Id_val.FIREDB);
            this.slowDownDebuff = new SlowDownDebuff(ID.Id_val.SLOWDOWNDB);

            Assert.AreEqual(this.player.speed, 2.0);
            this.slowDownDebuff.Effect(this.player);
            Assert.AreEqual(this.player.speed, 1.0);
            this.speedUpPowerup.Effect(this.player);            //speedup sostituisce lo slowdown
            Assert.AreNotEqual(this.player.speed, 1.0);
            Assert.AreNotEqual(this.player.speed, 2.0);
            Assert.AreEqual(this.player.speed, 4.0);
            this.fireDebuff.Effect(this.player);        
            Assert.AreEqual(this.player.health, 2);
            this.lifeUpPowerup.Effect(this.player);
            Assert.AreEqual(this.player.health, 3);
            this.invisiblePowerup.Effect(this.player);
            this.fireDebuff.Effect(this.player);        //con l'invisibilit� non perde vite il player
            this.fireDebuff.Effect(this.player);
            Assert.AreEqual(this.player.health, 3);
            this.speedUpPowerup.Effect(this.player);
            Assert.AreEqual(this.player.speed, 4.0);       // invisibilit� viene rimpiazzato da speedup, quindi perde vita col firedebuff
            this.fireDebuff.Effect(this.player);
            Assert.AreEqual(this.player.health, 2);
            this.knifePowerup.Effect(this.player);
            Assert.AreEqual(this.player.speed, 2.0);       //knife rimpiazza speedup, si torna a velocit� normale ma col coltello
            this.fireDebuff.Effect(this.player);
            Assert.AreEqual(this.player.health, 2);         //rimane a due vite
            this.slowDownDebuff.Effect(this.player);
            Assert.AreEqual(this.player.speed, 1.0);    
            Assert.IsFalse(this.player.hasKnife);       //slowdown rimpiazza knife che non c'� pi�
            this.lifeUpPowerup.Effect(this.player);
            Assert.AreEqual(this.player.health, 3);
            this.knifePowerup.Effect(this.player);
            Assert.IsTrue(this.player.isVisible);      //knifeup powerup d� invisibilit�
            this.invisiblePowerup.Effect(this.player);
            Assert.IsFalse(this.player.hasKnife);
            Assert.IsFalse(this.player.isVisible);
            this.fireDebuff.Effect(this.player);
            Assert.AreEqual(this.player.health, 3);
            this.invisiblePowerup.TerminateEffect(this.player);
            Assert.IsTrue(this.player.isVisible);
        }

        }
}