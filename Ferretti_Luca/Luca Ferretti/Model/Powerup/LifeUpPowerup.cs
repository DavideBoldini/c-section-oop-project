﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Luca_Ferretti.Luca_Ferretti.Model
{
    public class LifeUpPowerup
    {
        ID.Id_val id;

        public LifeUpPowerup(ID.Id_val ID)
        {
            this.id = ID;
        }
            

        public void Effect(Player player)
        {
            if (player.health < Player.MaxLifes)
            {
                player.AddLife();
            }
        }
    }


}
