﻿using System;
using System.Collections.Generic;
using System.Text;
using Luca_Ferretti.Luca_Ferretti.Controller;

namespace Luca_Ferretti.Luca_Ferretti.Model
{
    public class InvisiblePowerup : ActiveStaticObject, IPowerup
    {
        public InvisiblePowerup(ID.Id_val ID) 
            : base(ID) { }

        public void Effect(Player player)
        {
            this.ReplaceStaticObject(player);
            player.isVisible = false;
            player.ActivePowerupDebuff = this;
            this.limitedTimer = new LimitedTimer(player, this.id);
            this.limitedTimer.Start();
        }

        public override void TerminateEffect(Player player)
        {
            player.isVisible = true;
            player.ActivePowerupDebuff = null;
        }
    }
}
