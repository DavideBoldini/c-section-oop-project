﻿using System;
using System.Collections.Generic;
using Luca_Ferretti.Luca_Ferretti.Controller;
using System.Text;

namespace Luca_Ferretti.Luca_Ferretti.Model
{
    public class SpeedUpPowerup : ActiveStaticObject
    {
        const double multConstraint = 2;
        double oldSpeed { get; set; }

        public SpeedUpPowerup(ID.Id_val ID)
            : base(ID) { }


        public void Effect(Player player)
        {
            this.ReplaceStaticObject(player);
            player.ActivePowerupDebuff = this;
            this.oldSpeed = player.speed;
            player.speed = this.oldSpeed * multConstraint;
            this.limitedTimer = new LimitedTimer(player, this.id);
            this.limitedTimer.Start();
        }


        public override void TerminateEffect(Player player)
        {
            player.speed = this.oldSpeed;
            player.ActivePowerupDebuff = null;
        }
    }
}
