﻿using System;
using System.Collections.Generic;
using System.Text;
using Luca_Ferretti.Luca_Ferretti.Controller;

namespace Luca_Ferretti.Luca_Ferretti.Model
{
    public class KnifePowerup : ActiveStaticObject
    {

        public KnifePowerup(ID.Id_val ID)
            : base(ID) { }

        public void Effect(Player player)
        {
            this.ReplaceStaticObject(player);
            player.ActivePowerupDebuff = this;
            player.hasKnife = true;
            this.limitedTimer = new LimitedTimer(player, this.id);
            this.limitedTimer.Start();

        }

        public override void TerminateEffect(Player player)
        {
            player.ActivePowerupDebuff = null;
            player.hasKnife = false;
            if (player.ActivePowerupDebuff == null || player.ActivePowerupDebuff.id != ID.Id_val.INVISIBLEPU
                    || player.ActivePowerupDebuff.id != ID.Id_val.KNIFEPU)
            {
                player.isVisible = true;
            }
        }
    }
}
