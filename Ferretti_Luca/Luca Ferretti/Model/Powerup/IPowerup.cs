﻿using System;
using System.Collections.Generic;
using System.Text;
using Luca_Ferretti.Luca_Ferretti.Controller;

namespace Luca_Ferretti.Luca_Ferretti.Model
{
    interface IPowerup
    {

        void Effect(Player player);
    }
}
