﻿using System;
using System.Collections.Generic;
using System.Text;
using Luca_Ferretti.Luca_Ferretti.Controller;

namespace Luca_Ferretti.Luca_Ferretti.Model
{
    public abstract class ActiveStaticObject
    {
        public ID.Id_val id { get; set; }
        int posX { get; set; }
        int posY { get; set; }
        int velX { get; set; }
        int velY { get; set; }
        public LimitedTimer limitedTimer { get; set; }

        public ActiveStaticObject(ID.Id_val ID)
        {
            this.id = ID;
            this.posX = 0;
            this.posY = 0;
            this.velX = 0;
            this.velY = 0;
        }

        public void ReplaceStaticObject(Player player)
        {
            if (player.ActivePowerupDebuff != null)
            {
                player.ActivePowerupDebuff.limitedTimer.StopLimitedTimer();
            }
            player.ActivePowerupDebuff = null; ;
        }

        public abstract void TerminateEffect(Player player);

    }
}
