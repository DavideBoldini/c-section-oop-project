﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Luca_Ferretti.Luca_Ferretti.Model
{
    interface IDebuff
    {

        void Effect(Player player);

    }
}
