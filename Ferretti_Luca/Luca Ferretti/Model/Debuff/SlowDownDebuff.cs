﻿using System;
using System.Collections.Generic;
using System.Text;
using Luca_Ferretti.Luca_Ferretti.Controller;

namespace Luca_Ferretti.Luca_Ferretti.Model
{
    public class SlowDownDebuff : ActiveStaticObject, IDebuff
    {
        const double divConstraint = 2;
        double oldSpeed { get; set; }

        public SlowDownDebuff(ID.Id_val ID)
            : base(ID) { }

        public void Effect(Player player)
        {
            this.ReplaceStaticObject(player);
            player.ActivePowerupDebuff = this;
            this.oldSpeed = player.speed;
            player.speed = this.oldSpeed / divConstraint;
            this.limitedTimer = new LimitedTimer(player, this.id);
            this.limitedTimer.Start();
        }


        public override void TerminateEffect(Player player)
        {
            player.speed = this.oldSpeed;
            player.ActivePowerupDebuff = null;
        }

    }
}
