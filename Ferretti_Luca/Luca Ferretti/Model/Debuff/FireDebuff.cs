﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Luca_Ferretti.Luca_Ferretti.Model
{
    public class FireDebuff : IDebuff
    {
        ID.Id_val id;

        public FireDebuff(ID.Id_val ID)
        {
            this.id = ID;
        }

        public void Effect(Player player)
        {
            if (player.isVisible)
            {
                player.RemoveLife();
            }
        }

    }
}
