﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Luca_Ferretti.Luca_Ferretti.Model
{
    interface IPlayer
    {
        public void RemoveLife();

        public void AddLife();
    }
}
