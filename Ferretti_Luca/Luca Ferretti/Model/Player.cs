﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Luca_Ferretti.Luca_Ferretti.Model
{

    public class Player : IPlayer

    {
        public static int MaxLifes = 3;
        public double speed { get; set; }
        ID.Id_val id { get; set; }
        public int health { get; set; }
        public bool hasKnife { get; set; }
        public bool isVisible { get; set; }
        public bool gameOver { get; set; }
        public int velX { get; set; }
        public int velY { get; set; }
        public ActiveStaticObject ActivePowerupDebuff { get; set; }

        public Player(ID.Id_val ID, int velX, int velY)
        {
            this.id = ID;
            this.speed = 2.0;
            this.health = MaxLifes;
            this.isVisible = true;
            this.gameOver = false;
            this.velX = velX;
            this.velY = velY;
        }

        public void RemoveLife()
        {
            if (!this.hasKnife && this.isVisible)
            {
                this.health = this.health - 1;
            }
            if (this.health <= 0 && !this.gameOver)
            {
                this.gameOver = true;
            }
        }

        public void AddLife()
        {
            this.health = this.health + 1;
        }
    }
}
