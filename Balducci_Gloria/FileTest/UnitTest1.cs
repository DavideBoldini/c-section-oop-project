﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FileTest
{

    [TestClass]
    public class WriteFileTests
    {
        /// <summary>
        /// Test that verify the true creation of the file Ranking
        /// </summary>
        [TestMethod]
        public void CreateRankingFile_FileCreated_AssertEquals()
        {
            var sut = new WriteFile();

            sut.CreateRankingFile();

            //StreamReader is able to read a file only if the file already exists
            var sr1 = new System.IO.StreamReader(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + "\\" + "ranking");

            string outputLines1 = sr1.ReadLine();

            Assert.Equals("Top 3 of level 1", outputLines1);
        }

        /// <summary>
        /// Test that verify the true creation of the file Times
        /// </summary>
        [TestMethod]
        public void CreateTimesFile_FileCreated_AssertEquals()
        {
            var sut = new WriteFile();

            sut.CreateTimesFile();

            //StreamReader is able to read a file only if the file already exists

            var sr2 = new System.IO.StreamReader(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + "\\" + "times");

            string outputLines2 = sr2.ReadLine();
    
            Assert.Equals("Top 3 of level 1", outputLines2);
        }

        /// <summary>
        /// Test that verify the true creation of the file Times
        /// </summary>
        [TestMethod]
        public void CreateScoresFile_FileCreated_AssertEquals()
        {
            var sut = new WriteFile();

            sut.CreateScoresFile();

            //StreamReader is able to read a file only if the file already exists
             var sr3 = new System.IO.StreamReader(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + "\\" + "scores");

             string outputLines3 = sr3.ReadLine();

            Assert.Equals("", outputLines3);
        }
    }
}
