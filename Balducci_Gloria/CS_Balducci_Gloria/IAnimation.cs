﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;



interface IAnimation
{
    /// <summary>
    /// Method to inizialize first frame
    /// </summary>
    /// <param name="list"> list of image</param>
    void Init(List<Bitmap> list);

    /// <summary>
    /// Method to run animation with the next frame
    /// </summary>
    /// <param name="list">list of image</param>
    void RunAnimation(List<Bitmap> list);

    /// <summary>
    /// Method to get next frame image
    /// </summary>
    /// <param name="list"> list of image</param>
    void NextFrame(List<Bitmap> list);

    /// <summary>
    /// Method to draw the animation
    /// </summary>
    /// <param name="g">where to draw the image</param>
    /// <param name="x">coordinate x</param>
    /// <param name="y">coordinate y</param>
    void DrawAnimation(Graphics g, int x, int y);
}

