﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

class Animation : IAnimation
{
    private readonly int speed;
    private int index;
    private int count;
    private Bitmap current;
   
    public void Init(List<Bitmap> list)
    {
        this.current = list[0];
    }

    public void RunAnimation(List<Bitmap> list)
    {
        index++;
        if (index > speed)
        {
            index = 0;
            NextFrame(list);
        }
    }

    public void NextFrame(List<Bitmap> list)
    {
        for (int i = 0; i < list.Capacity; i++)
        {
            if (count == i)
            {
                current = list[i];
            }
        }
        count++;
        if (count > list.Capacity)
        {
            count = 0;
        }
    }

    public void DrawAnimation(Graphics g, int x, int y)
    {
        g.DrawImage(current, x, y);
    }
}

