﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Abstractation;


public class WriteFile : IWriteFile
{
    public static readonly String FILE_NAME_RANKING = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + "\\" + "ranking";

    public static readonly String FILE_NAME_TIMES = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + "\\" + "times";

    public static readonly String FILE_NAME_SCORES = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + "\\" + "scores";

    public void CreateRankingFile()
    {
        try
        {
            Console.WriteLine(FILE_NAME_RANKING);

            if (!System.IO.File.Exists(FILE_NAME_RANKING))
            {
                using (System.IO.StreamWriter outputFile = new System.IO.StreamWriter(FILE_NAME_RANKING))
                {
                    outputFile.WriteLine("Top 3 of level 1" + Environment.NewLine + "no time for first lv.1" + Environment.NewLine
                            + "no time for second lv.1" + Environment.NewLine + "no time for third lv.1" + Environment.NewLine
                            + Environment.NewLine + "Top 3 of level 2" + Environment.NewLine + "no time for first lv.2"
                            + Environment.NewLine + "no time for second lv.2" + Environment.NewLine + "no time for third lv.2"
                            + Environment.NewLine + Environment.NewLine + "Top 3 of level 3" + Environment.NewLine
                            + "no time for first lv.3" + Environment.NewLine + "no time for second lv.3" + Environment.NewLine
                            + "no time for third lv.3" + Environment.NewLine + Environment.NewLine + "Top 3 of level 4"
                            + Environment.NewLine + "no time for first lv.4" + Environment.NewLine + "no time for second lv.4"
                            + Environment.NewLine + "no time for third lv.4" + Environment.NewLine + Environment.NewLine
                            + "Top 3 of level 5" + Environment.NewLine + "no time for first lv.5" + Environment.NewLine
                            + "no time for second lv.5" + Environment.NewLine + "no time for third lv.5");
                }
            }
        }
        catch (System.IO.IOException e)
        {
            e.GetBaseException();
        }

    }

    public void CreateTimesFile()
    {
        try
        {
            Console.WriteLine(FILE_NAME_TIMES);

            if (!System.IO.File.Exists(FILE_NAME_TIMES))
            {
                using (System.IO.StreamWriter outputFile = new System.IO.StreamWriter(FILE_NAME_TIMES))
                {
                    outputFile.WriteLine("no time for first lv.1" + Environment.NewLine + "no time for second lv.1" + Environment.NewLine
                            + "no time for third lv.1" + Environment.NewLine + Environment.NewLine

                            + "no time for first lv.2" + Environment.NewLine + "no time for second lv.2" + Environment.NewLine
                            + "no time for third lv.2" + Environment.NewLine + Environment.NewLine

                            + "no time for first lv.3" + Environment.NewLine + "no time for second lv.3" + Environment.NewLine
                            + "no time for third lv.3" + Environment.NewLine + Environment.NewLine

                            + "no time for first lv.4" + Environment.NewLine + "no time for second lv.4" + Environment.NewLine
                            + "no time for third lv.4" + Environment.NewLine + Environment.NewLine

                            + "no time for first lv.5" + Environment.NewLine + "no time for second lv.5" + Environment.NewLine
                            + "no time for third lv.5");
                }
            }
        }
        catch (System.IO.IOException e)
        {
            e.GetBaseException();
        }

    }

    public void CreateScoresFile()
    {
        try
        {
            Console.WriteLine(FILE_NAME_SCORES);

            if (!System.IO.File.Exists(FILE_NAME_SCORES))
            {
                using (System.IO.StreamWriter outputFile = new System.IO.StreamWriter(FILE_NAME_SCORES))
                {
                    outputFile.WriteLine("");
                }
            }
        }
        catch (System.IO.IOException e)
        {
            e.GetBaseException();
        }

    }
}

