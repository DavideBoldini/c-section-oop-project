﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


interface IWriteFile
{
    /// <summary>
    /// Write Ranking file if it doesn't exist
    /// </summary>
    void CreateRankingFile();

    /// <summary>
    /// Write Times file if it doesn't exist
    /// </summary>
    void CreateTimesFile();

    /// <summary>
    /// Write Scores file if it doesn't exist
    /// </summary>
    void CreateScoresFile();


}

