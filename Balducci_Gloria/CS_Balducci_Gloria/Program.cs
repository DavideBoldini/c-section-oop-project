﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

    class Program
    {
    public static readonly WriteFile wf = new WriteFile();
   
    static void Main(string[] args)
    {
        wf.CreateRankingFile();
        wf.CreateTimesFile();
        wf.CreateScoresFile();
    }
}

