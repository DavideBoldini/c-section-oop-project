using CSharpOOP;
using NUnit.Framework;
using System;
using System.Drawing;
using System.Linq;

namespace AssertTest
{
    //Classe di NUnitTest per verificare i seguenti meccanismi:
    //1) Corretto caricamento dell'immagine all'istanziazione di un'entit�.
    //2) Corretto funzionamento dei meccanismi base della classe Handler (Aggiunta entit�, rimozione entit�, ricerca entit�)
    [TestFixture]
    public class AssertTest
    {
        private GetTexture texture;
        private Handler handler;
        private SupportEnemy support;
        private TowerEnemy tower;
        private Wall wall;

        //Metodo di setup (default NUnit) atto all'inizializzazione delle variabili a noi necessarie
        [SetUp]
        public void Setup()
        {
            this.handler = new Handler();
            this.texture = new GetTexture();

            //Creazione entit� SupportEnemy con valori di default: 
            //Posizione iniziale (0,0), 
            //Velocit� iniziale nulla (0.0, 0.0) ed 
            //immagini necessarie per il caricamento della texture ricavate dalla classe GetTexture
            //NOTA: E' stata scelta appositamente questa entit� essendo essa senza Ray ed essendo un'entit� col principio di movimento
            this.support = new SupportEnemy(ID.ID_VAL.ENEMY, handler, 0, 0, 0.0, 0.0, texture.supportEnemyListTexture);

            //Creazione entit� TowerEnemy con valori di default: 
            //Posizione iniziale (0,0), 
            //Velocit� iniziale nulla (0.0, 0.0) ed 
            //immagini necessarie per il caricamento della texture ricavate dalla classe GetTexture
            //NOTA: Si tratta di un'entit� "statica" non avendo principi di movimento e avente un Ray
            this.tower = new TowerEnemy(ID.ID_VAL.ENEMY, 0, 0, 0.0, 0.0, texture.towerEnemyImage, texture.towerEnemyRayImage);

            //Creazione entit� Wall con valori di default:
            //Posizione iniziale (0,0)
            this.wall = new Wall(ID.ID_VAL.WALL, 0, 0, texture.wallListTexture[0]);
        }

        //Metodo atto alla verifica della corretta presenza delle texture nelle varie entit�
        [Test]
        public void TestTexture()
        {
            //Prima Verifica: si verifica se effettivamente � stata correttamente caricata la texture
            Assert.NotNull(wall.image);
            Assert.NotNull(support.imagesDictionary);
            Assert.NotNull(tower.image);
            Assert.NotNull(tower.RayEntity.image);

            //Seconda Verifica: si verifica che per quanto riguarda le entit� di movimento (nel nostro caso SupportEnemy)
            //le diverse texture dei vari movimenti siano state caricate correttamente
            Assert.AreEqual(support.GetTextureByDirection(Direction.Direction_Val.UP).Count, 3);
            Assert.AreEqual(support.GetTextureByDirection(Direction.Direction_Val.DOWN).Count, 3);
            Assert.AreEqual(support.GetTextureByDirection(Direction.Direction_Val.LEFT).Count, 3);
            Assert.AreEqual(support.GetTextureByDirection(Direction.Direction_Val.RIGHT).Count, 3);

            //Terza Verifica: si verifica se l'inizializzazione della dimensione di un'entit� si � basata sull'effettiva dimensione della texture
            //Tale verifica avverr� analizzando tutte le texture per ogni movimento
            foreach (Direction.Direction_Val direction in Enum.GetValues(typeof(Direction.Direction_Val)))
            {
                foreach (Bitmap img in support.GetTextureByDirection(direction))
                {
                    Assert.AreEqual(img.Width, support.dimension.Key);
                    Assert.AreEqual(img.Height, support.dimension.Value);
                }
            }
        }

        //Metodo atto alla verifica del corretto lancio di eccezioni nel caso di operazioni inopportune
        [Test]
        public void TestException()
        {
            //Verifica del corretto lancio di un'eccezione nel caso si richiedesse la lista delle texture relative ad un movimento ad un'entit� "statica"

            //Prima Verifica: TowerEnemy
            Assert.Throws<NullReferenceException>(() => tower.GetTextureByDirection(Direction.Direction_Val.UP));
            Assert.Throws<NullReferenceException>(() => tower.GetTextureByDirection(Direction.Direction_Val.DOWN));
            Assert.Throws<NullReferenceException>(() => tower.GetTextureByDirection(Direction.Direction_Val.LEFT));
            Assert.Throws<NullReferenceException>(() => tower.GetTextureByDirection(Direction.Direction_Val.RIGHT));

            //Seconda Verifica: Wall
            Assert.Throws<NullReferenceException>(() => wall.GetTextureByDirection(Direction.Direction_Val.UP));
            Assert.Throws<NullReferenceException>(() => wall.GetTextureByDirection(Direction.Direction_Val.DOWN));
            Assert.Throws<NullReferenceException>(() => wall.GetTextureByDirection(Direction.Direction_Val.LEFT));
            Assert.Throws<NullReferenceException>(() => wall.GetTextureByDirection(Direction.Direction_Val.RIGHT));
        }

        //Metodo atto alla verifica dei meccanismi base dell'Handler
        [Test]
        public void TestHandler()
        {
            //Controllo dimensione lista iniziale -> Lista vuota
            Assert.IsTrue(!handler.GameObjectList.Any());

            //Aggiunta alla lista dell'Handler delle tre entit� prima inizializzate
            this.handler.addGameObject(tower);
            this.handler.addGameObject(support);
            this.handler.addGameObject(wall);

            //Verifica della dimensione della lista/e dopo l'inserimento
            Assert.IsFalse(!handler.GameObjectList.Any());
            Assert.AreEqual(this.handler.GameObjectList.Count, 3);
            Assert.AreEqual(this.handler.GetEnemyList().Count, 2);

            Assert.IsFalse(!handler.GetWallList().Any());
            Assert.AreEqual(handler.GetWallList().Count, 1);

            //Rimozione oggetti dalla lista
            this.handler.removeGameObject(tower);
            this.handler.removeGameObject(wall);

            //Verifica della dimensione della lista/e dopo la rimozione
            Assert.IsFalse(!handler.GameObjectList.Any());
            Assert.IsTrue(!handler.GetWallList().Any());
            Assert.AreEqual(handler.GameObjectList.Count, 1);
        }
    }
}