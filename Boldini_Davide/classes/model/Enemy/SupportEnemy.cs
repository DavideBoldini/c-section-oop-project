﻿using System.Collections.Generic;
using System.Drawing;

namespace CSharpOOP
{
    public class SupportEnemy : Enemy
    {
        private Handler handler;

        public SupportEnemy(ID.ID_VAL id, Handler handler, int posX, int posY, double velX, double velY, List<KeyValuePair<Direction.Direction_Val, Bitmap>> image)
            : base(id, posX, posY, velX, velY, image)
        {
            this.handler = handler;
        }

        public override void Tick()
        {
        }
    }
}