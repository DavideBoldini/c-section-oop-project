﻿using System.Collections.Generic;
using System.Drawing;

//NOTA: Implementazione Parziale
namespace CSharpOOP
{
    public abstract class Enemy : GameObject
    {
        public Ray RayEntity { get; }
        private List<Bitmap> listUp;
        private List<Bitmap> listDown;
        private List<Bitmap> listLeft;
        private List<Bitmap> listRight;

        public Enemy(ID.ID_VAL id, int posX, int posY, double velX, double velY, List<KeyValuePair<Direction.Direction_Val, Bitmap>> textureImages)
            : base(id, posX, posY, velX, velY, textureImages)
        {
            this.listUp = base.GetTextureByDirection(Direction.Direction_Val.UP);
            this.listDown = base.GetTextureByDirection(Direction.Direction_Val.DOWN);
            this.listLeft = base.GetTextureByDirection(Direction.Direction_Val.LEFT);
            this.listRight = base.GetTextureByDirection(Direction.Direction_Val.RIGHT);
        }

        public Enemy(ID.ID_VAL id, int posX, int posY, double velX, double velY, List<KeyValuePair<Direction.Direction_Val, Bitmap>> textureImages, List<KeyValuePair<Direction.Direction_Val, Bitmap>> rayImages)
            : base(id, posX, posY, velX, velY, textureImages)
        {
            this.RayEntity = new Ray(ID.ID_VAL.RAY, posX, posY, 0, 0, rayImages);
            this.listUp = base.GetTextureByDirection(Direction.Direction_Val.UP);
            this.listDown = base.GetTextureByDirection(Direction.Direction_Val.DOWN);
            this.listLeft = base.GetTextureByDirection(Direction.Direction_Val.LEFT);
            this.listRight = base.GetTextureByDirection(Direction.Direction_Val.RIGHT);
        }

        public Enemy(ID.ID_VAL id, int posX, int posY, double velX, double velY,
            Bitmap image, Bitmap rayImage) : base(id, posX, posY, velX, velY, image)
        {
            this.RayEntity = new Ray(ID.ID_VAL.RAY, posX, posY, 0, 0, rayImage);
        }

        public class Ray : GameObject
        {
            public Ray(ID.ID_VAL id, int posX, int posY, double velX, double velY, List<KeyValuePair<Direction.Direction_Val, Bitmap>> rayList) : base(id, posX, posY, velX, velY, rayList)
            {
            }

            public Ray(ID.ID_VAL id, int posX, int posY, double velX, double velY, Bitmap rayImage) : base(id, posX, posY, velX, velY, rayImage)
            {
            }

            public override void Tick()
            {
            }
        }
    }
}