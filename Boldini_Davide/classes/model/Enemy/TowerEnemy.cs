﻿using System.Drawing;

namespace CSharpOOP
{
    public class TowerEnemy : Enemy
    {
        public TowerEnemy(ID.ID_VAL id, int posX, int posY, double velX, double velY, Bitmap image, Bitmap rayImage)
            : base(id, posX, posY, velX, velY, image, rayImage)
        {
        }

        public override void Tick()
        {
        }
    }
}