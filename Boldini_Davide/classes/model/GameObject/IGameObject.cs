﻿using System.Collections.Generic;
using System.Drawing;

namespace CSharpOOP
{
    internal interface IGameObject
    {
        void SetImages(List<KeyValuePair<Direction.Direction_Val, Bitmap>> images);
        List<Bitmap> GetListUP();
        List<Bitmap> GetListDOWN();
        List<Bitmap> GetListLEFT();
        List<Bitmap> GetListRIGHT();
        void SetImage(Bitmap newImage);
        List<Bitmap> GetTextureByDirection(Direction.Direction_Val direction);
        void Tick();

    }
}