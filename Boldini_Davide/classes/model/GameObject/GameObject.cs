﻿using System.Collections.Generic;
using System.Drawing;

//NOTA: Implementazione Parziale

namespace CSharpOOP
{
    public abstract class GameObject : IGameObject
    {
        public bool visible { get; set; }
        public KeyValuePair<int, int> coordinates { get; set; }
        public KeyValuePair<double, double> velocity { get; set; }
        public KeyValuePair<int, int> dimension { get; set; }
        public ID.ID_VAL id { get; }
        public Dictionary<Direction.Direction_Val, List<Bitmap>> imagesDictionary { get; }

        public Bitmap image { get; set; }

        public GameObject(ID.ID_VAL id, int posX, int posY, double velX, double velY, List<KeyValuePair<Direction.Direction_Val, Bitmap>> newImages)
        {
            this.id = id;
            this.coordinates = new KeyValuePair<int, int>(posX, posY);
            this.velocity = new KeyValuePair<double, double>(velX, velY);
            this.imagesDictionary = new Dictionary<Direction.Direction_Val, List<Bitmap>>();

            this.FillDictionaryImages(newImages);

            this.dimension = new KeyValuePair<int, int>(newImages[0].Value.Width, newImages[0].Value.Height);
            this.visible = true;
        }

        //Costruttore con Immagine unica
        public GameObject(ID.ID_VAL id, int posX, int posY, double velX, double velY, Bitmap image)
        {
            this.id = id;
            this.coordinates = new KeyValuePair<int, int>(posX, posY);
            this.velocity = new KeyValuePair<double, double>(velX, velY);
            this.image = image;
            this.dimension = new KeyValuePair<int, int>(this.image.Width, this.image.Height);
            this.visible = true;
        }

        private void FillDictionaryImages(List<KeyValuePair<Direction.Direction_Val, Bitmap>> list)
        {
            this.imagesDictionary.Add(Direction.Direction_Val.UP, new List<Bitmap>());
            this.imagesDictionary.Add(Direction.Direction_Val.DOWN, new List<Bitmap>());
            this.imagesDictionary.Add(Direction.Direction_Val.LEFT, new List<Bitmap>());
            this.imagesDictionary.Add(Direction.Direction_Val.RIGHT, new List<Bitmap>());

            foreach (KeyValuePair<Direction.Direction_Val, Bitmap> val in list)
            {
                if (val.Key.Equals(Direction.Direction_Val.UP))
                {
                    imagesDictionary[val.Key].Add(val.Value);
                }
                else if (val.Key.Equals(Direction.Direction_Val.DOWN))
                {
                    imagesDictionary[val.Key].Add(val.Value);
                }
                else if (val.Key.Equals(Direction.Direction_Val.LEFT))
                {
                    imagesDictionary[val.Key].Add(val.Value);
                }
                else if (val.Key.Equals(Direction.Direction_Val.RIGHT))
                {
                    imagesDictionary[val.Key].Add(val.Value);
                }
            }
        }

        public void SetImages(List<KeyValuePair<Direction.Direction_Val, Bitmap>> images)
        {
            imagesDictionary.Clear();
            this.FillDictionaryImages(images);
        }

        public List<Bitmap> GetListUP()
        {
            return this.imagesDictionary[Direction.Direction_Val.UP];
        }

        public List<Bitmap> GetListDOWN()
        {
            return this.imagesDictionary[Direction.Direction_Val.DOWN];
        }

        public List<Bitmap> GetListLEFT()
        {
            return this.imagesDictionary[Direction.Direction_Val.LEFT];
        }

        public List<Bitmap> GetListRIGHT()
        {
            return this.imagesDictionary[Direction.Direction_Val.RIGHT];
        }

        public void SetImage(Bitmap newImage)
        {
            this.image = newImage;
        }

        public List<Bitmap> GetTextureByDirection(Direction.Direction_Val direction)
        {
            List<Bitmap> finalList = new List<Bitmap>();
            foreach (KeyValuePair<Direction.Direction_Val, List<Bitmap>> entry in imagesDictionary)
            {
                if (entry.Key.Equals(direction))
                {
                    finalList.AddRange(entry.Value);
                }
            }
            return finalList;
        }

        public abstract void Tick();
    }
}