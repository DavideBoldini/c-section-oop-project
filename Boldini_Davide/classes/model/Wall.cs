﻿using System.Drawing;

namespace CSharpOOP
{
    public class Wall : GameObject
    {
        public Wall(ID.ID_VAL id, int posX, int posY, Bitmap image) : base(id, posX, posY, 0, 0, image)
        {
        }

        public override void Tick()
        {
        }
    }
}