﻿using System.Collections.Generic;
using System.Drawing;

namespace CSharpOOP
{
    internal interface IImageLoader
    {
        Bitmap GetImageByRowColumn(Bitmap image, int row, int column, int width, int height);

        List<Bitmap> GetImageByRow(Bitmap image, int row, int width, int height);

        List<Bitmap> GetImageByColumn(Bitmap image, int column, int width, int height);

        List<KeyValuePair<Direction.Direction_Val, Bitmap>> TextureByDirectionList(Bitmap image, Direction.Direction_Val direction, int row, int width, int height);
    }
}