﻿using System.Collections.Generic;
using System.Drawing;

//NOTA: Implementazione Parziale

namespace CSharpOOP
{
    public class ImageLoader : IImageLoader
    {
        public Bitmap GetImageByRowColumn(Bitmap image, int row, int column, int width, int height)
        {
            Rectangle rect = new Rectangle(column * width, row * height, width, height);
            return image.Clone(rect, image.PixelFormat);
        }

        public List<Bitmap> GetImageByRow(Bitmap image, int row, int width, int height)
        {
            List<Bitmap> listImage = new List<Bitmap>();
            int column = 0;
            for (int i = 0; i < image.Width; i = i + width)
            {
                listImage.Add(this.GetImageByRowColumn(image, row, column, width, height));
                column++;
            }
            return listImage;
        }

        public List<Bitmap> GetImageByColumn(Bitmap image, int column, int width, int height)
        {
            List<Bitmap> listImage = new List<Bitmap>();
            int row = 0;
            for (int i = 0; i < image.Height; i = i + height)
            {
                listImage.Add(this.GetImageByRowColumn(image, row, column, width, height));
                row++;
            }
            return listImage;
        }

        public List<KeyValuePair<Direction.Direction_Val, Bitmap>> TextureByDirectionList(Bitmap image, Direction.Direction_Val direction, int row, int width, int height)
        {
            List<KeyValuePair<Direction.Direction_Val, Bitmap>> finalList = new List<KeyValuePair<Direction.Direction_Val, Bitmap>>();
            this.GetImageByRow(image, row, width, height).ForEach(x => finalList.Add(new KeyValuePair<Direction.Direction_Val, Bitmap>(direction, x)));
            return finalList;
        }
    }
}