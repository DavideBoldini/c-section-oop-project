﻿using System.Collections.Generic;

//NOTA: Implementazione Parziale

namespace CSharpOOP
{
    public class Handler : IHandler
    {
        public List<GameObject> GameObjectList { get; }

        public Handler()
        {
            this.GameObjectList = new List<GameObject>();
        }

        public void removeGameObject(GameObject toRemove)
        {
            GameObjectList.RemoveAll(item => item == toRemove);
        }

        public void addGameObject(GameObject toAdd)
        {
            GameObjectList.Add(toAdd);
        }

        public List<GameObject> GetEnemyList()
        {
            return GameObjectList.FindAll(item => item.id.Equals(ID.ID_VAL.ENEMY));
        }

        public List<GameObject> GetWallList()
        {
            return GameObjectList.FindAll(item => item.id.Equals(ID.ID_VAL.WALL));
        }
    }
}