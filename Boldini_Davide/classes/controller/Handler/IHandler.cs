﻿using System.Collections.Generic;

namespace CSharpOOP
{
    internal interface IHandler
    {
        void removeGameObject(GameObject toRemove);

        void addGameObject(GameObject toAdd);

        List<GameObject> GetEnemyList();

        List<GameObject> GetWallList();
    }
}