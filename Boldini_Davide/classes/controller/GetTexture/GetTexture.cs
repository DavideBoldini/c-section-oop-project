using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;

//NOTA: Implementazione Parziale
namespace CSharpOOP
{
    public class GetTexture
    {
        private static readonly List<Direction.Direction_Val> MOVEMENT = new List<Direction.Direction_Val>(Enum.GetValues(typeof(Direction.Direction_Val)).Cast<Direction.Direction_Val>().ToList());

        private KeyValuePair<int, int> sizePlayerEnemiesPair;
        private KeyValuePair<int, int> sizeElemTilesheetPair;

        private readonly ImageLoader loadImage;

        private Bitmap tilesheetTexture;

        public Bitmap towerEnemyImage;

        public Bitmap towerEnemyRayImage { get; private set; }

        public List<KeyValuePair<Direction.Direction_Val, Bitmap>> supportEnemyListTexture { get; private set; }

        public List<Bitmap> chestListTexture { get; } // chestListTexture[0] = chest1,
                                                      // chestListTexture[1] = chest2,
                                                      // chestListTexture[2] = chest3

        public List<Bitmap> wallListTexture { get; } // wallListTexture[0] = Wall1,
                                                     // wallListTexture[1] = Wall2,
                                                     // wallListTexture[2] = Wall3

        public List<Bitmap> levelListStructure { get; } // levelListStructure[0] = level1,
                                                        // levelListStructure[1] = level2,
                                                        // levelListStructure[2] = level3

        public GetTexture()
        {
            this.sizePlayerEnemiesPair = new KeyValuePair<int, int>(32, 48);
            this.sizeElemTilesheetPair = new KeyValuePair<int, int>(64, 64);

            this.loadImage = new ImageLoader();

            this.supportEnemyListTexture = new List<KeyValuePair<Direction.Direction_Val, Bitmap>>();
            this.chestListTexture = new List<Bitmap>();
            this.wallListTexture = new List<Bitmap>();
            this.levelListStructure = new List<Bitmap>();

            this.Init();
        }

        public void Init()
        {
            this.SetTilesheet();
            this.SetSupportEnemyImage();
            this.SetTowerEnemyImage();
            this.SetTowerEnemyRayImage();
            this.SetWallImage();
        }

        private void SetTilesheet()
        {
            try
            {
                tilesheetTexture = new Bitmap(CSharpOOP.Properties.Resources.tileSheets);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        private void SetSupportEnemyImage()
        {
            Bitmap temp = null;
            try
            {
                temp = new Bitmap(CSharpOOP.Properties.Resources.SupportEnemy);
            }
            catch (Exception e)
            {
                Console.Write(e);
            }

            if (temp != null)
            {
                supportEnemyListTexture.AddRange(this.loadImage.TextureByDirectionList(temp, MOVEMENT[3], 0, sizePlayerEnemiesPair.Key, sizePlayerEnemiesPair.Key));
                supportEnemyListTexture.AddRange(this.loadImage.TextureByDirectionList(temp, MOVEMENT[2], 1, sizePlayerEnemiesPair.Key, sizePlayerEnemiesPair.Key));
                supportEnemyListTexture.AddRange(this.loadImage.TextureByDirectionList(temp, MOVEMENT[1], 2, sizePlayerEnemiesPair.Key, sizePlayerEnemiesPair.Key));
                supportEnemyListTexture.AddRange(this.loadImage.TextureByDirectionList(temp, MOVEMENT[0], 3, sizePlayerEnemiesPair.Key, sizePlayerEnemiesPair.Key));
            }
        }

        private void SetTowerEnemyImage()
        {
            try
            {
                this.towerEnemyImage = new Bitmap(CSharpOOP.Properties.Resources.TowerEnemy);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        private void SetTowerEnemyRayImage()
        {
            try
            {
                this.towerEnemyRayImage = new Bitmap(CSharpOOP.Properties.Resources.ray4);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        //NOTA: Metodo Parziale
        private void SetWallImage()
        {
            this.wallListTexture.Add(loadImage.GetImageByRowColumn(this.tilesheetTexture, 2, 11, sizeElemTilesheetPair.Key, sizeElemTilesheetPair.Value));
        }
    }
}