﻿namespace CSharpOOP
{
    internal interface IGetTexture
    {
        void Init();

        void SetTilesheet();

        void SetPlayerImage();

        void SetBaseEnemyImage();

        void SetWallImage();
    }
}